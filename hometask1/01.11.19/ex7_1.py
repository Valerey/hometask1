import time

def my_time(f_t):
    def wrapped(*args , **kwargs):
        start_time =  time.time()
        m_d = f_t(*args, **kwargs)
        print("Work time =",time.time()-start_time)
        return m_d
    return wrapped

 
 
@my_time
def  my_sum(m_n):
    m_sum = 0
    for my_cnt in range(m_n):
        m_sum += my_cnt
    return m_sum



k=my_sum(11111)
print(k)

