def tri_nu(my_count):
    """ 
        Generated triangular numbers
    """
    for my_cnt in range(my_count):
        yield (my_cnt + 1) * my_cnt // 2

for my_cnt in tri_nu(40):
    print(my_cnt,)

