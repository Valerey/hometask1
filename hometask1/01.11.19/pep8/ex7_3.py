def my_skip(f_t):
    def wrapped(*args, **kwargs):
        m_d = 10
        return m_d
    return wrapped


@my_skip
def my_sum(m_n):
    m_sum = 0
    for my_cnt in range(m_n):
        m_sum += my_cnt
    return m_sum

k = my_sum(11111)
print(k)
