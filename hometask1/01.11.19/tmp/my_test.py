import pickle
import json
import time
import os


 def cache(suffix=''):
    
         def inner(func_to_wrapp):
    
             def wrapper(*args, **kwargs):
                 cache_name = f'{"_".join([str(x) for x in args])}{suffix}.cache'
                 if os.path.exists(cache_name):
                     with open(cache_name, 'rb') as cache:
                         return pickle.load(cache)
    
                 data = func_to_wrapp(*args, **kwargs)
    
                 with open(cache_name, 'wb') as cache:
                     pickle.dump(data, cache)
    
                 return data
    
             return wrapper
    
         return inner
    
 @cache()
     def some_long_time_method(x, y):
         time.sleep(10)
         return x + y
    
     # @cache(suffix='div')
     def some_long_time_method1(x, y):
         time.sleep(10)
         return x - y
    
     @cache()
     def some_long_time_method2(x, y, t):
         time.sleep(10)
         return x - y + t
    
     print(some_long_time_method1(2, 2))
     some_long_time_method2(2, 2, 2)

