from tkinter import *
 
root = Tk()
 

op_1 = Entry(width=30)
op_2 = Entry(width=30)
my_b = Button(text="+",width=5,height=1)
my_b1= Button(text="-",width=5,height=1)
my_b2= Button(text="*",width=5,height=1)
my_b3= Button(text="/",width=5,height=1)



my_l = Label(bg='black', fg='white', width=30)
 
def plus(event):
    s_op1 = op_1.get()
    s_op2 = op_2.get()
    try:
        my_l['text'] = str(float(s_op1)+float(s_op2))
    except:
        my_l['text'] = 'ERROR'

def minus(event):
    s_op1 = op_1.get()
    s_op2 = op_2.get()
    try:
        my_l['text'] = str(float(s_op1)-float(s_op2))
    except:
        my_l['text'] = 'ERROR'


def mul(event):
    s_op1 = op_1.get()
    s_op2 = op_2.get()
    try:
        my_l['text'] = str(float(s_op1)*float(s_op2))
    except:
        my_l['text'] = 'ERROR'

def div(event):
    s_op1 = op_1.get()
    s_op2 = op_2.get()
    try:
        my_l['text'] = str(float(s_op1)/float(s_op2))
    except:
        my_l['text'] = 'ERROR'

my_b.bind('<Button-1>',plus)
my_b1.bind('<Button-1>',minus)
my_b2.bind('<Button-1>',mul)
my_b3.bind('<Button-1>',div)
op_1.pack()
op_2.pack()
my_b.pack()
my_b1.pack()
my_b2.pack()
my_b3.pack()
my_l.pack()
root.mainloop()
