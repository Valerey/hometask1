import os
import asyncio

my_sum = 0
my_mul = 1
my_square = 0

async def cal(my_file):

    with open(f"data/{my_file}", "r") as my_file:
        my_str = my_file.read()
    my_list = my_str.split()
    if my_list[0] == "1":
        global my_sum
        my_sum += float(my_list[1]) + float(my_list[2]) + float(my_list[3])
    elif my_list[0] == "2":
        global my_mul
        my_mul *= float(my_list[1]) * float(my_list[2]) * float(my_list[3])
    elif my_list[0] == "3":
        global my_square
        my_square += float(my_list[1]) * float(my_list[1]) + \
            float(my_list[2]) * float(my_list[2]) + \
            float(my_list[3]) * float(my_list[3])

async def asynchronous():
    my_files = os.listdir("data")

    for my_file in my_files:
        await cal(my_file)

    print (my_sum, "===", my_mul, "====", my_square)
    with open("out.dat", "w") as my_file:
        my_file.write("sum=" + str(my_sum) +
                      " \nmul=" + str(my_mul) + "\nsquare=" + str(my_square))


ioloop = asyncio.get_event_loop()
ioloop.run_until_complete(asynchronous())
ioloop.close()
