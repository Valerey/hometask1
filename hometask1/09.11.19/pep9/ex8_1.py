
class Lorry:

    type = None
    mass = None
    company = None
    speed = None

    def __init__(self, type, mass, company, speed):
        self.type = type
        self.mass = mass
        self.company = company
        self.speed = speed

    def __del__(self):
        print(f'Goodbay  Lorry {self.type}')

    def info(self):
        print("Lorry")
        print("Type =", self.type)
        print("Mass =", self.mass)
        print("Speed =", self.speed)
        print("Company =", self.company)

    def lorry_loading(self, bulk_mass):
        self.mass += bulk_mass
        return self.mass

    def lorry_unloading(self, bulk_mass):
        self.mass -= bulk_mass
        return self.mass

    def lorry_rented(self, company):
        self.company = company
        print(f' Lorry is rented {self.company} company ')


class Ship:

    type = None
    deadweight = None
    sh_class = None
    speed = None
    company = None

    def __init__(self, type, deadweight, sh_class, speed, company):
        self.type = type
        self.deadweight = deadweight
        self.sh_class = sh_class
        self.speed = speed
        self.company = company

    def __del__(self):
        print(f'Goodbay ship {self.type}')

    def info(self):
        print("Ship")
        print("Type =", self.type)
        print("Deadweight =", self.deadweight)
        print("Sh_class =", self.sh_class)
        print("Speed =", self.speed)
        print("Company =", self.company)

    def ship_loading(self, bulk_mass):
        self.deadweight += bulk_mass
        return self.deadweight

    def ship_unloading(self, bulk_mass):
        self.deadweight -= bulk_mass
        return self.deadweight

    def freightage(self, company):
        self.company = company
        print(f' Ship is chartered {self.company} company ')


class Airfreighter:

    type = None
    g_weight = None
    speed = None
    company = None

    def __init__(self, type, g_weight, speed, company):
        self.type = type
        self.g_weight = g_weight
        self.speed = speed
        self.company = company

    def __del__(self):
        print(f'Goodbay plane {self.type}')

    def info(self):
        print("Airfreighter")
        print("Type =", self.type)
        print("Gross weight =", self.g_weight)
        print("Speed =", self.speed)
        print("Company =", self.company)

    def airfreighter_loading(self, bulk_mass):
        self.g_weight += bulk_mass
        return self.g_weight

    def airfreighter_unloading(self, bulk_mass):
        self.g_weight -= bulk_mass
        return self.g_weight

    def airfreighter_rented(self, company):
        self.company = company
        print(f' Airfreighter is rented {self.company} company ')


class AmphibiousTruck:
    type = None
    mass = None
    speed = None
    company = None

    def __init__(self, type, mass, speed, company):
        self.type = type
        self.mass = mass
        self.speed = speed
        self.company = company

    def __del__(self):
        print(f'Goodbay amphibious truck  {self.type}')

    def info(self):
        print("AmphibiousTruck")
        print("Type =", self.type)
        print("Mass =", self.mass)
        print("Speed =", self.speed)
        print("Company =", self.company)

    def amphibioustruck_loading(self, bulk_mass):
        self.mass += bulk_mass
        return self.mass

    def amphibioustruck_unloading(self, bulk_mass):
        self.mass -= bulk_mass
        return self.mass

    def amphibioustruck_rented(self, company):
        self.company = company
        print(f' Amphibious truck is rented {self.company} company ')

TopSucsess = Ship('Top Sucsess', 20000, 'sea',  20, 'sea line')
TopSucsess.info()
print(TopSucsess.ship_loading(20))
TopSucsess.info()
TopSucsess.freightage('Sea Fleet')
TopSucsess.ship_unloading(50)
print(TopSucsess.ship_unloading(50))
TopSucsess.info()

Man = Lorry('Man320', 15000, 'Avtodor', 70)
Man.info()
print (Man.lorry_loading(3200))
Man.info()
print (Man.lorry_unloading(2200))
Man.info()
Man.lorry_rented('Railway comp')

A320 = Airfreighter('A320', 110000, 924, 'UIA')
A320.info()
print (A320.airfreighter_loading(41000))
A320.info()
print (A320.airfreighter_unloading(5200))
A320.info()
A320.airfreighter_rented('Airbus')

XM158 = AmphibiousTruck('XM-158 Drake', 2800, 45, 'Road')
XM158.info()
print(XM158.amphibioustruck_loading(720))
XM158.info()
print(XM158.amphibioustruck_unloading(200))
XM158.info()
XM158.amphibioustruck_rented('Private persons')
