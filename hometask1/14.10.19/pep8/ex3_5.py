first_digit = None
first_number = input("first number =")
flag_minus = 1
if first_number[0] == "-":
    flag_minus = -1
    first_number = first_number.lstrip("-")
float_flag = first_number.find(".")
if (float_flag == -1 and first_number.isdigit()):
    first_digit = int(first_number) * flag_minus
elif ((float_flag != -1) and
      (first_number[:float_flag]+first_number[float_flag+1:]).isdigit()):
    first_digit = float(first_number) * flag_minus
else:
    print ("Invalid data")

second_digit = None
second_number = input("second number =")
flag_minus = 1
if second_number[0] == "-":
    flag_minus = -1
    second_number = second_number.lstrip("-")
float_flag = second_number.find(".")
if (float_flag == -1 and second_number.isdigit()):
    second_digit = int(second_number) * flag_minus
elif ((float_flag != -1) and
      (second_number[:float_flag]+second_number[float_flag+1:]).isdigit()):
    second_digit = float(second_number) * flag_minus
else:
    print ("Invalid data")


third_digit = None
third_number = input("third number =")
flag_minus = 1
if third_number[0] == "-":
    flag_minus = -1
    third_number = third_number.lstrip("-")
float_flag = third_number.find(".")
if (float_flag == -1 and third_number.isdigit()):
    third_digit = int(third_number) * flag_minus
elif ((float_flag != -1) and
      (third_number[:float_flag]+third_number[float_flag+1:]).isdigit()):
    third_digit = float(third_number) * flag_minus
else:
    print ("Invalid data")


if (first_digit-second_digit + third_digit) != 0:
    print("Res =", 2*first_digit-8 * second_digit /
          (first_digit-second_digit+third_digit))
else:
    print("Devide by zero")
