my_string = input("input number =")
flag_minus = 1
if my_string[0] == "-":
    flag_minus = -1
    my_string = my_string.lstrip("-")
flag = 0

if my_string.isdigit():
    my_number = int(my_string)
    for cont in range(2, my_number // 2 + 1):
        if my_number % cont == 0:
            flag += 1
    if flag == 0:
        print(my_number * flag_minus, "=  is  prime number")
    else:
        print(my_number * flag_minus, "=  is composite number")
else:
        print("Invalid data")
