
class Home_lib:
    def __init__(self):
        self = []

    def insert_book(self, author, title, pub_year, type, locate, book_id, mem):
        d = dict(author=author, title=title, pub_year=pub_year,
                 type=type, locate=locate, book_id=book_id)
        mem.append(d)

    def sort_pub_year(self, mem):
        mem.sort(key=lambda x: x['pub_year'])

    def sort_author(self, mem):
        mem.sort(key=lambda x: x['author'])

    def sort_title(self, mem):
        mem.sort(key=lambda x: x['title'])

    def find_title(self, title, mem):
        return list(filter(lambda x: x['title'] == title, k))

    def find_author(self, author, mem):
        return list(filter(lambda x: x['author'] == author, k))

    def find_pub_year(self, pub_year, mem):
        return list(filter(lambda x: x['pub_year'] == pub_year, k))

    def del_book(self, book_id, mem):
        for cnt_mem in range(len(mem)-1):
            if mem[cnt_mem]['book_id'] == book_id:
                print(mem[cnt_mem], "\n", "======= deleted")
                mem.pop(cnt_mem)

    def print_lib(self, mem):
        for l_s in mem:
            print(l_s)

k = []

c = Home_lib()

c.insert_book('Knoot1', 'Programming', 1968, 'book', 'shelf1', '684',  k)
c.insert_book('Knoot2', 'Programming2', 1963, 'book', 'shelf1', 'A215', k)
c.insert_book('Knoot3', 'Programming1', 1961, 'book', 'shelf1', 'B564', k)

c.print_lib(k)
print('=====')
c.sort_title(k)
c.print_lib(k)
print('=====')
c.sort_author(k)
c.print_lib(k)
print('=====')
print(c.find_pub_year(1963, k))
print('=====')
c.del_book('684', k)
c.print_lib(k)
