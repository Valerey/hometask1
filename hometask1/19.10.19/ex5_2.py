def cesare_cript (shift,text_in):
    text_out=""
    CONST_ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    for count in range (len(text_in)) :
        posision_my = CONST_ABC.find(text_in[count])

        if  posision_my  == -1 :
            text_out += text_in[count]
        else:
            text_out += CONST_ABC[(posision_my + shift) % 52]

    return (text_out)

def file_name(in_file):
    in_legal =("!@$&%*()[]{}'? ") + (':;><`"')+chr(ord("#"))

    for inl_chr in in_legal:
        if (in_file.find(inl_chr)) != -1:
            return (False)

    return (True)



in_file = input("Name input file--->")

if not file_name(in_file):
    print("Invalid file name")
    exit(0)

with open (in_file) as hand_file :
    text_in=hand_file.read()

my_string = (input("Shift ="))

if not (my_string.isdigit()):
    print("Invalid number")
    exit(0)
shift = int(my_string)


text_out = cesare_cript(shift,text_in)

out_file = input("Name output file--->")
if not file_name(out_file):
    print("Invalid file name")
    exit(0)




with open(out_file, "w") as hand_file:
    hand_file.write(text_out)




