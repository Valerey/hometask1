import pickle


class Event:
    def __init__(self):
        self = []

    def buy_clos(self, id, price, count, day, mounth, year, mem):
        d = dict(id=id, price=price, count=count,
                 day=day, month=mounth, year=year)
        mem.append(d)

    def day_log(self, day, mounth, year, mem):

        receipts = 0
        expenditures = 0
        for i in list(filter(lambda thing: thing['day'] == day and
                      thing['month'] == mounth and 
                      thing['year'] == year, mem)):
            if i['id'] > 0:
                receipts += i['price'] * i['count']
            elif i['id'] < 0:
                expenditures += i['price'] * i['count']
        print('daily report    ', day, "-", mounth, '-', year)
        print('receipts = ', receipts, "   ", 'expenditures =', expenditures)
        print("profit =", receipts - expenditures)

    def mounth_log(self, mounth, year, mem):

        receipts = 0
        expenditures = 0
        for i in list(filter(lambda thing: thing['month'] == mounth and
                      thing['year'] == year, mem)):
            if i['id'] > 0:
                receipts += i['price'] * i['count']
            elif i['id'] < 0:
                expenditures += i['price'] * i['count']
        print('monthly report    ', mounth, '-', year)
        print('receipts = ', receipts, "   ", 'expenditures =', expenditures)
        print("profit =", receipts - expenditures)

    def year_log(self, year, mem):

        receipts = 0
        expenditures = 0
        for i in list(filter(lambda thing: thing['year'] == year, mem)):
            if i['id'] > 0:
                receipts += i['price'] * i['count']
            elif i['id'] < 0:
                expenditures += i['price'] * i['count']
        print('annual report    ', year)
        print('receipts = ', receipts, "   ", 'expenditures =', expenditures)
        print("profit =", receipts - expenditures)

with open('My_log', 'rb') as f:
    thing = pickle.load(f)
c = Event()

c.day_log(1, 11, 2019, thing)
c.mounth_log(11, 2019, thing)
c.year_log(2019, thing)

c.buy_clos(21, 100, 1, 2, 11, 2019, thing)
c.buy_clos(22, 100, 1, 2, 10, 2019, thing)
c.buy_clos(23, 100, 1, 2, 11, 2018, thing)

c.day_log(1, 11, 2019, thing)
c.mounth_log(11, 2019, thing)
c.year_log(2019, thing)
c.day_log(2, 11, 2019, thing)
c.mounth_log(10, 2019, thing)
c.year_log(2018, thing)
# with open('My_log', 'wb') as f:
#     pickle.dump(thing,f)
