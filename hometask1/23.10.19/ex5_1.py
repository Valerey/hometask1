def input_integer(string="my_numb ="):
    my_string = (input(string))

    if not (my_string.isdigit()):
        print("Invalid file name")
        exit(0)
    my_number = int(my_string)
    return (my_number) 


ROMA_DIGIT = [
["M",1000, ], ["CM", 900], ["D", 500, ], ["CD", 400, ],  ["C", 100, ], ["XC", 90, ], ["L", 50], ["XL", 40],
["X", 10], ["IX", 9, ], ["V", 5, ], ["IV", 4, ], ["I", 1, ], ]


ar_num = input_integer("Arabic number")
roma_number = ""

while ar_num > 0:

    for r_str,r_val in ROMA_DIGIT : 
        if ar_num >= r_val :
            roma_number += r_str
            ar_num -= r_val
            break



print (roma_number)



