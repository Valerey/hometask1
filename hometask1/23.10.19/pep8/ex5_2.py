def file_name(in_file):
    in_legal = ("!@$&%*()[]{}'? ") + (':;><`"')+chr(ord("#"))

    for inl_chr in in_legal:
        if (in_file.find(inl_chr)) != -1:
            return (False)

    return (True)


def no_numbers(in_file):

    if not file_name(in_file):
        print("Invalid file name")
        exit(0)
    with open(in_file) as hand_file:
        text_in = hand_file.read()
    for cn_t in range(10):
        out_text = text_in. replace(str(cn_t), "")
        text_in = out_text

    with open(in_file, "w") as hand_file:
        hand_file.write(out_text)
in_file = input("Name input file--->")
no_numbers(in_file)
