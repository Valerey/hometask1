import unittest


def tri_nu(my_count):
    """.
        Generated triangular numbers
    """
    for my_cnt in range(my_count):
        yield (my_cnt + 1) * my_cnt // 2


class Test(unittest.TestCase):

    def test_tri_nu(self):

        def list__tri_nu(my_count):
            tri_list = []
            for my_cnt in tri_nu(my_count):
                tri_list.append(my_cnt)
            return tri_list
            self.assertEqual(list_tri_nu(5), [0, 1, 3, 6, 10])
            self.assertEqual(list_tri_nu(10), [0, 1, 3, 6, 10,
                             15, 21, 28, 36, 45])


if __name__ == "__main__":
    unittest.main()
