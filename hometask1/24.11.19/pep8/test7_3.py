import unittest


def Geom_pr(my_numb):
    my_lst = my_numb * [0]
    for my_cnt in range(my_numb):
        my_lst[-1 * (my_cnt+1)] = (my_cnt+1) * 3
    return my_lst 


class Test(unittest.TestCase):

    def test_tri_nu(self):

        self.assertEqual(Geom_pr(5), [15, 12, 9, 6, 3])
        self.assertEqual(Geom_pr(10), [30, 27, 24, 21, 18, 15, 12, 9, 6, 3])


if __name__ == "__main__":
    unittest.main()
