import unittest


def my_skip(f_t):
    def wrapped(*args, **kwargs):
        m_d = 10
        return m_d
    return wrapped


@my_skip
def my_sum(m_n):
    m_sum = 0
    for my_cnt in range(m_n):
        m_sum += my_cnt
    return m_sum


class Test(unittest.TestCase):

    def test_tri_nu(self):

        self.assertEqual(my_sum(120), 10)
        self.assertEqual(my_sum(30), 10)


if __name__ == "__main__":
    unittest.main()

