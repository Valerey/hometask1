import time

def my_time(f_t):
    def wrapped(*args , **kwargs):
        start_time =  time.time()
        m_d = f_t(*args, **kwargs)
        return time.time()-start_time
    return wrapped

@my_time
def  my_sum(m_n):
    time.sleep(m_n)
    return  





import unittest

class Test(unittest.TestCase):
    def test_time(self):
        @my_time
        def  my_sum(m_n):
            time.sleep(m_n)
            return 
        self.assertEqual(int(my_sum(2)), 2)
        self.assertEqual(int(my_sum(4)), 4)

if __name__ == "__main__":
    unittest.main()
