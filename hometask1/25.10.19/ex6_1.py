
import re
def file_name(in_file):
    in_legal =("!@$&%*()[]{}'? ") + (':;><`"')+chr(ord("#"))

    for inl_chr in in_legal:
        if (in_file.find(inl_chr)) != -1:
            return (False)

    return (True)
in_file = input("Name input file--->")

if not file_name(in_file):
    print("Invalid file name")
    exit(0)





with open (in_file) as hand_file :
    text_in=hand_file.read()
date_in_text = []

for m in re.finditer(r'\d\d\-\d\d-\d{4}', text_in):
    my_str = re.split('-',m.group())
    my_day = int(my_str[0]) ; my_mounth = int(my_str[1]) ; my_year=int(my_str[2])
    leaf_year = not (my_year % 400) or (my_year % 100) and not(my_year %4)
    valid_data = (my_mounth == 1 and my_day <= 31) or (my_mounth == 2 and  my_day <= (28+leaf_year)) or \
                  (my_mounth == 3 and my_day <=31) or (my_mounth == 4 and my_day <= 30) or \
                  (my_mounth == 5 and my_day <=31) or (my_mounth == 6 and my_day <= 30) or \
                  (my_mounth == 7 and my_day <=31) or (my_mounth == 8 and my_day <= 31) or \
                  (my_mounth == 9 and my_day <=30) or (my_mounth == 10 and my_day <= 31) or \
                  (my_mounth == 11 and my_day <=30) or (my_mounth == 12 and my_day <= 31)
    if valid_data :
        date_in_text.append([my_str[0],my_str[1],my_str[2]])
print(date_in_text)


