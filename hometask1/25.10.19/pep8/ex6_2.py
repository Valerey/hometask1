from re import match
my_string = input("Year= ")
year = match(r'\d\d\d\d', my_string)
if(year is None):
    print ("ERROR")
    exit(0)
my_year = int(year.group())
if(not (my_year % 400) or (my_year % 100) and not (my_year % 4)):
    print("Year ", my_year, " is leap")
else:
    print("Year ", my_year, " is regular")
