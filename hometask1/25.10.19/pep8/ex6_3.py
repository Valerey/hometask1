import re


def file_name(in_file):
    in_legal = ("!@$&%*()[]{}'? ") + (':;><`"')+chr(ord("#"))

    for inl_chr in in_legal:
        if (in_file.find(inl_chr)) != -1:
            return (False)

    return (True)

in_file = input("Name input file--->")

if not file_name(in_file):
    print("Invalid file name")
    exit(0)
with open(in_file) as hand_file:
    text_in = hand_file.read()
my_parser = re.findall(r'-?\d+\.?\d*', text_in)
print(my_parser)
my_sum = 0.
for my_ind in my_parser:
    my_sum += float(my_ind)
print("Sum =", my_sum)
