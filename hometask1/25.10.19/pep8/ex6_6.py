import re


def file_name(in_file):
    in_legal = ("!@$&%*()[]{}'? ") + (':;><`"')+chr(ord("#"))

    for inl_chr in in_legal:
        if (in_file.find(inl_chr)) != -1:
            return (False)

    return (True)
in_file = input("Name input file--->")

if not file_name(in_file):
    print("Invalid file name")
    exit(0)
with open(in_file) as hand_file:
    my_text = hand_file.read()

my_result = re.sub(r'[.!?]\s+[A-Z]\w+', "", my_text)
my_res = re.findall(r'\b[A-Z][a-z]\w*', my_result)
print (my_res)
